# MAPS Assist & Evaluations Control

## Problem

There are 8 workshops and for each student MAPS must save the assistance and two grades: one for the activities and another one for the exams. Every workshop has a different topic as shown in the README.md file. The algorithm has to provide a method to save the data and to calculate an individual workshop grade, and assistance grade and the workshop grade average.


## Solution


### Assumptions

* Every event slot is going to have a timestamp indicating the time when the event was created.
* Every event has an assistance key.
* Arrays positions start at 0.
* Code was written in javascript but has not been tested with actual data, it could have errors but the logic is there, treat it as pseudo-code.
* Code calls auxiliary functions. Those has not been written but functionaly is explained.


### Functions

#### Constants:

* smart_card_size: 40;
* event_max_slots: 5;
* max_assistance: 40;

#### Auxilary Function: Get Latest Event

**Inputs:** EventsArray

**Output:**: ArrayIndex:int

Function sorts the events array and returns index of the event with the highest timestamp.
Function returns -1 if EventsArray is empty.

#### Auxiliary Function: Object Key Count

**Input:**  JSONObject

**Output:**  count:int

Function iterates the JSON object and counts how many keys the object has.

#### Add New Assistance

**Inputs:** EventsArray:array.

**Output:** EventsArray:array.

1. Get the events array length;
2. If the current size is 0 (card is empty) the event with assistant is going to be inserted in position 0.
3. Else, the event is going to be inserted in the next slot available.

Code:

```
function add_assistance(eventsArray){
	var arrayLength = eventsArray.length;
	var nextIndex; // stores the index for the new assistance event.
	var lastEvent; // stores the data of the last event stored in the card.
	if arrayLength == 0{
		new_event = {'asistencia': 1}	
		eventsArray[0] = new_event;
		return eventsArray;
	}
	// smart card is not full, next slot is easy determinated by events array size. 
	if arrayLength < smart_card_size{ // (smart card not full)
	  	nextIndex = arrayLength;
	  	lastEvent = eventsArray[arrayLength-1]
	  	// same results could be achieved by the else clause but having this condition prevents an unnecessary search.
	}
	else{ //smart card is full, 
		var lastEventIndex = getLatestEvent(eventsArray); // from the auxiliary function.
		nextIndex = (lastEventIndex == (smart_card_size-1)?0:lastEventIndex+1); // since the card has circular storage, when the last possible index is achieved, next element must be saved at the start of array.
		lastEvent = eventsArray[lastEventIndex];
	}
	// Data of lastEvent is duplicated in the new record but the value of asistencia increases by one and the timestamp shows the datetime of the operation.
	lastEvent.asistencia = lastEvent.asistencia++;
	lastEvent.timestamp = now(); 
	eventsArray[nextIndex] = lastEvent;
	return eventsArray;
}
```
#### Add New Grade

**Input:** eventsArray, gradeObject

**Output:** eventsArray

1. Perform of gradeObject validation: object must have just one key and it's value must be a decimal between 0 and 10.
2. Perorm of eventsArray validation: due to restrictions, assistance cannot be added on the fly. Logically there cannot be a grade without an assistance so the user must add an assistance first. Hence, eventsArray cannot be empty in order to proceed the execution of this function.
3. The function retrieves the latest event object by doing a search of its index with the help of the auxiliary function.
4. If the object is full, meaning it has 5 slots of actual data, the algorithm must search if the gradeObject key is there, if it is, the lastest event is going to be cloned and the key value is going to be replaced, if the key is not there, a new event will be created, its object will consist of the gradeObject key-value.
5. For each case above, the assistance value will be copied from the latest event and the timestamp will be determined by the time of the operation.

Code:

```
function add_grade(eventsArray, gradeObject){
	if(validateGrateObject(gradeObject) && eventsArray.length>0){
		var lastEventIndex = getLatestEvent(EventsArray); // with the help of the auxiliary function
		var lastEvent = eventsArray[lastEventIndex];
		var keyCount = objectKeyCount(lastEvent); 
		var gradeName = Object.keys(gradeObject)[0]; //already validated that gradeObject has just one key.
		var newEvent;
		if(gradeName in lastEvent){
			// latest event contains the grade of gradeObject. Object will be copied but value of gradeName key is going to be updated.
			newEvent = lastEvent;
			newEvent[gradeName] = gradeObject[gradeName];		
		}
		else{
			if(keyCount<event_max_slots){
				newEvent = lastEvent;
				newEvent[gradeName] = gradeObject[gradeName];	
			}
			else{
				newEvent = {'asistencia': lastEvent.asistencia}
			}
		}
		newEvent[gradeName] = gradeObject[gradeName];
		newEvent[timestamp] = now();
		var nextIndex = (lastEventIndex == (smart_card_size-1)?0:lastEventIndex+1);
		eventsArray[nextIndex] = newEvent;		
	}
	return eventsArray;
}
```

#### List Processing

**Input:** EventsArray

**Output:** GradesObject

1. Sets a list of keys the eventsArray could have. This lists is based on the name of the 8 workshops + activities.
2. The function retrieves the latest event object by doing a search of its index with the help of the auxiliary function.
3. The function creates a return object with the proper keys but with null values.
4. The events array values are iterated from end to start, beginning from the index found in step 2. Max number of iterations is set by the smart_card_size constant value.
	1. The list of keys is iterated.
	2. if the key name is found in the event object, the return object is filled with the data and the key name is removed from the list because its latest value has been found.
	3. The events array iterator breaks if all key are found.
5. For each workshop (key in return object) the average grade is calculated and this averaged is added to the grade great summatory.
6. Assistance data is added to the return object. Events Array assisted is easy retrieved from the latest event found in step 2.
7. Assistance grade is added to the grades summatory.
8. Final note of workshops + assistance is added to return object.

Code:

```
function processList(eventsArray){
	var keyList = [
		'salud_actividad','salud_examen',
		'nutricion_actividad, nutricion_examen', 
		'arte_actividad', 'arte_examen', 
		'educacion_actividad', 'educacion_examen', 
		'autoestima_actividad', 'autoestima_examen', 
		'empleabilidad_actividad', 'empleabilidad_examen', 
		'cliente_actividad','client_examen', 
		'mercadeo_actividad', 'mercadeo_examen'
	]
	var lastEventIndex = getLatestEvent(EventsArray); // with the help of the auxiliary function
	var lastEvent = eventsArray[lastEventIndex];
	var iterator = 0, iteratorIndex = lastEventIndex;
	var returnObj = {		
		'salud':{'examen': null, 'actividad': null, 'nota': null},
		'nutricion':{'examen': null, 'actividad': null, 'nota': null},
		'arte':{'examen': null, 'actividad': null, 'nota': null},
		'educacion':{'examen': null, 'actividad': null, 'nota': null},
		'autoestima':{'examen': null, 'actividad': null, 'nota': null},
		'empleabilidad':{'examen': null, 'actividad': null, 'nota': null},
		'cliente':{'examen': null, 'actividad': null, 'nota': null},
		'mercadeo':{'examen': null, 'actividad': null, 'nota': null},
	}
	while(iterator < smart_card_size){
		var event = eventsArray[iteratorIndex];
		
		for(var i=0;i<keyList.length;i++){
			if(keyList[i] in event){
				//key is found, returnObj must be filled with the data.
				var gradeArr = keyList[i].split("_"); //0: workshop_name, 1: grade_name
				returnObj[gradeArr[0]][gradeArr[1]] = event[keyList[i]];
				keyList = keyList.splice(i,1); // key is removed from list so the function doesn't search it anymore.
			}
		}
		if(keyList.length==0) break; // if all keys has been found iterator must end.
		
		iteratorIndex = iteratorIndex== 0?(smart_card_size-1):(iteratorIndex-1);
		iterator++;
	}
	// All data has been set in returnObj, now function has to calculate averages.
	var sumGrades = 0;
	for (key in returnObj){
		var grade = (returnObj[key].examen + returnObj[key].actividad)/2;
		returnObj[key].nota = grade;
		sumGrade += grade;
	}
	returnObj['asistencia'] = {
		'asistencia': lastEvent['asistencia'], 
		'total': max_assistance, 
		'nota': lastEvent['asistencia']/max_assistance*10;
	}
	sumGrades += returnObj['asistencia'].nota;
	returnObj['nota_final'] = sumGrades/9;

	// optional: remove keys with null values from returnObj output.

	return returnObj;
}

```
